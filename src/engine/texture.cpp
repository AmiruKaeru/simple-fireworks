#include "texture.h"

Texture::Texture()
    : ID_(0)
    , width_(0)
    , height_(0)
    , internalFormat(GL_RGB)
    , imageFormat(GL_RGB)
    , wrapS(GL_REPEAT)
    , wrapT(GL_REPEAT)
    , filterMin(GL_LINEAR)
    , filterMax(GL_LINEAR)
{
    glGenTextures(1, &ID_);
}

void Texture::setInternalFormat(const GLenum &value)
{
    internalFormat = value;
}

void Texture::setImageFormat(const GLenum &value)
{
    imageFormat = value;
}

GLuint Texture::getID()
{
    return ID_;
}

GLuint Texture::getWidth() const
{
    return width_;
}

GLuint Texture::getHeight() const
{
    return height_;
}

void Texture::generate(GLuint width, GLuint height, unsigned char *data)
{
    width_ = width;
    height_ = height;

    glBindTexture(GL_TEXTURE_2D, ID_);
    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, imageFormat, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMin);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMax);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::bind() const
{
    glBindTexture(GL_TEXTURE_2D, ID_);
}