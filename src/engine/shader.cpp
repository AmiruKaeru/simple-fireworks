#include "shader.h"

#include <iostream>
#include <vector>
#include <glm/gtc/type_ptr.hpp>

Shader::Shader(const GLchar *vertexSource, const GLchar *fragmentSource)
    : ID_(0)
{
    compile(vertexSource, fragmentSource);
}

Shader &Shader::use()
{
    glUseProgram(ID_);
    return *this;
}

void Shader::compile(const GLchar *vertexSource, const GLchar *fragmentSource)
{
    GLuint sVertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(sVertex, 1, &vertexSource, NULL);
    glCompileShader(sVertex);
    checkCompileErrors(sVertex, VERTEX);

    GLuint sFragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(sFragment, 1, &fragmentSource, NULL);
    glCompileShader(sFragment);
    checkCompileErrors(sFragment, FRAGMENT);

    ID_ = glCreateProgram();
    glAttachShader(ID_, sVertex);
    glAttachShader(ID_, sFragment);
    glLinkProgram(ID_);
    checkCompileErrors(ID_, PROGRAM);

    glDeleteShader(sVertex);
    glDeleteShader(sFragment);
}

void Shader::setVector2f(const GLchar *name, GLfloat x, GLfloat y, GLboolean useShader)
{
    if (useShader)
    {
        use();
    }
    glUniform2f(glGetUniformLocation(ID_, name), x, y);
}

void Shader::setVector2f(const GLchar *name, const glm::vec2 &value, GLboolean useShader)
{
    if (useShader)
    {
        use();
    }
    glUniform2f(glGetUniformLocation(ID_, name), value.x, value.y);
}

void Shader::setVector4f(const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader)
{
    if (useShader)
    {
        use();
    }
    glUniform4f(glGetUniformLocation(ID_, name), x, y, z, w);
}

void Shader::setVector4f(const GLchar *name, const glm::vec4 &value, GLboolean useShader)
{
    if (useShader)
    {
        use();
    }
    glUniform4f(glGetUniformLocation(ID_, name), value.x, value.y, value.z, value.w);
}

void Shader::setMatrix4(const GLchar * name, const glm::mat4 & value, GLboolean useShader)
{
    if (useShader)
    {
        use();
    }
    glUniformMatrix4fv(glGetUniformLocation(ID_, name), 1, GL_FALSE, glm::value_ptr(value));
}

GLuint Shader::getID() const
{
    return ID_;
}

void Shader::checkCompileErrors(const GLuint &object, const Type &type)
{
    GLint success = 0;
    GLint log_length = 0;
    std::vector<GLchar> info_log;

    switch (type)
    {
    case VERTEX:
    case FRAGMENT:
    {
        glGetShaderiv(object, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
            info_log.resize(log_length + 1);
            glGetShaderInfoLog(object, log_length, nullptr, info_log.data());
            std::cerr << "[ERROR] Shader Compilation Error | Type: " << (type == VERTEX ? "Vertex" : "Fragment")
                << "\n" << info_log.data() << std::endl;
        }
        break;
    }
    case PROGRAM:
    {
        glGetProgramiv(object, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
            info_log.resize(log_length + 1);
            glGetProgramInfoLog(object, log_length, nullptr, info_log.data());
            std::cerr << "[ERROR] Shader Program Linking Error \n" << info_log.data() << std::endl;
        }
        break;
    }
    }
}
