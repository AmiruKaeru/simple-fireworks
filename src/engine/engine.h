#pragma once

#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <map>

#include "firework_manager.h"
#include "cloud_manager.h"
#include "background.h"

class Engine
{
public:
    Engine();

    void run();

private:
    GLFWwindow *window_;
    FireworkManager fireworks;
    CloudManager clouds;
    Background nightsky;

    void init();
    void mainLoop();
    void cleanUp();
};