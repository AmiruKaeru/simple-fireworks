#pragma once

#include <list>

#include "utils.h"
#include "resource_manager.h"
#include "particle_manager.h"

class CloudManager : public ParticleManager
{
public:
    CloudManager();

    void setScreenSize(const int &width, const int &height);

    void setup() override;

    void update() override;
    void draw() override;

    void clear() override;
private:
    struct Cloud : public Particle
    {
    private:
        int textureID;
    public:
        GLfloat direction;
        Texture sprite;

        void resetPosition(float screenWidth, float screenHeight)
        {
            position = glm::vec2((direction > 0.0f ? 0.0f - sprite.getWidth() : screenWidth), generateRandom(0.0f, screenHeight));
        }

        Cloud() : Particle(), textureID(0), direction(0), sprite() { }
        Cloud(const glm::vec2 &position, const glm::vec2 &velocity, const glm::vec4 &color, const GLfloat &life, const GLfloat &direction)
            : Particle(position, velocity, color, life)
            , textureID(generateRandom(1, 3))
            , direction(direction)
            , sprite(ResourceManager::loadTexture(std::string(getPath() + "\\textures\\cloud" + std::to_string(textureID) + ".png").c_str(), GL_TRUE, std::string("cloud" + textureID).c_str()))
            { }
    };
    std::list<Cloud> clouds;

    float screenWidth;
    float screenHeight;
};