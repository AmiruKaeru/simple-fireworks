﻿#include "firework_manager.h"

#include "resource_manager.h"

FireworkManager::FireworkManager()
    : ParticleManager()
{
}

void FireworkManager::setup()
{
    ResourceManager::loadShaderFromSource(R"(
        #version 330 core
        layout (location = 0) in vec2 aPos;

        out vec4 aColor;

        uniform mat4 projection;
        uniform vec2 offset;
        uniform vec4 color;

        void main()
        {
            float scale = 10.0f;
            aColor = color;
            gl_Position = projection * vec4((aPos * scale) + offset, 0.0, 1.0);
        }
    )", R"(
        #version 330 core
        in vec4 aColor;
        out vec4 fragColor;

        void main()
        {
            fragColor = aColor;
        }
    )", "firework");

    GLfloat vertex_quad[] = {
        0.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    glGenVertexArrays(1, &VAO_);
    glGenBuffers(1, &VBO_);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_quad), vertex_quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), static_cast<GLvoid *>(0));
    glBindVertexArray(0);
}

void FireworkManager::spawn(const glm::vec2 &mousePosition, const GLfloat &screenHeight)
{
    fireworks.emplace_back(glm::vec2(mousePosition.x, screenHeight), randomVec2(7.5f, 15.5f), glm::vec4(randomVec3(0.0f, 1.0f), 1.0f), 1.0f, mousePosition.y);
}

void FireworkManager::update()
{
    for (auto &firework : fireworks)
    {
        if (firework.position.y > firework.mousePosition.y)
        {
            firework.position.y -= firework.velocity.y;
        }
        else
        {
            if (firework.sparks.empty())
            {
                for (int i = 0; i < 100; ++i)
                {
                    firework.sparks.emplace_back(firework.position, firework.velocity, firework.color, 1.0f);
                }
                firework.color.a = 0.0f;
            }
            bool sparksIsDead = true;

            for (auto &spark : firework.sparks)
            {
                spark.life -= generateRandom(0.001f, 0.06f);

                if (spark.life > 0.0f) 
                {
                    sparksIsDead = false;
                    spark.position += spark.direction * spark.velocity;
                    spark.color.a = spark.life;
                }
            }

            if (sparksIsDead)
            {
                firework.life = 0.0f;
            }
        }
    }

    fireworks.remove_if([](Firework firework) {
        return firework.life <= 0.0f;
    });
}

void FireworkManager::draw()
{
    auto shader = ResourceManager::getShader("firework").use();
    for (auto firework : fireworks)
    {
        shader.setVector2f("offset", firework.position);
        shader.setVector4f("color", firework.color);
        glBindVertexArray(VAO_);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);

        for (auto spark : firework.sparks)
        {
            shader.setVector2f("offset", spark.position);
            shader.setVector4f("color", spark.color);
            glBindVertexArray(VAO_);
            glDrawArrays(GL_TRIANGLES, 0, 6);
            glBindVertexArray(0);
        }
    }
}

void FireworkManager::clear()
{
    glDeleteVertexArrays(1, &VAO_);
    glDeleteBuffers(1, &VBO_);
}