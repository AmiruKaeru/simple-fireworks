#include "engine.h"

#include <iostream>
#include <glm/ext/matrix_clip_space.hpp>

#include "resource_manager.h"

Engine::Engine()
{
}

void Engine::run()
{
    init();
    mainLoop();
    cleanUp();
}

void setViewport(const int &width, const int &height)
{
    glViewport(0, 0, width, height);
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.0f, -1.0f, 1.0f);
    ResourceManager::getShader("firework").use().setMatrix4("projection", projection);
    ResourceManager::getShader("cloud").use().setMatrix4("projection", projection);
    ResourceManager::getShader("background").use().setMatrix4("projection", projection);
}

void Engine::init()
{
    glfwSetErrorCallback([](int error, const char *description)
    {
        std::cerr << "GLFW error [" << error << "]: " << description << std::endl;
    });

    if (!glfwInit())
    {
        throw std::runtime_error("Failed to initialize!");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    int screenWidth = 1280;
    int screenHeight = 720;
    window_ = glfwCreateWindow(1280, 720, "Fireworks", nullptr, nullptr);
    if (!window_)
    {
        throw std::runtime_error("Failed to create window!");
    }

    glfwMakeContextCurrent(window_);
    if (gl3wInit())
    {
        throw std::runtime_error("Failed to initialize OpenGL!");
    }

    glfwSwapInterval(1);

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback([](GLenum source,
                                GLenum type,
                                GLuint id,
                                GLenum severity,
                                GLsizei length,
                                const GLchar *message,
                                const void *userParam)
    {
        std::cerr << "[GL ERROR]\n" << message << std::endl;
    }, 0);

    fireworks.setup();
    clouds.setScreenSize(screenWidth, screenHeight);
    clouds.setup();
    nightsky.setup();
    setViewport(screenWidth, screenHeight);
    glfwSetFramebufferSizeCallback(window_, [](GLFWwindow *window, int width, int height)
    {
        setViewport(width, height);
    });
    
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
}

void Engine::mainLoop()
{
    while (!glfwWindowShouldClose(window_))
    {
        GLenum err;
        while((err = glGetError()) != GL_NO_ERROR)
        {
            std::string error;
            switch(err)
            {
            case GL_INVALID_ENUM:
                error = "GL_INVALID_ENUM";
                break;
            case GL_INVALID_VALUE:
                error = "GL_INVALID_VALUE";
                break;
            case GL_INVALID_OPERATION:
                error = "GL_INVALID_OPERATION";
                break;
            case GL_STACK_OVERFLOW:
                error = "GL_STACK_OVERFLOW";
                break;
            case GL_STACK_UNDERFLOW:
                error = "GL_STACK_UNDERFLOW";
                break;
            case GL_OUT_OF_MEMORY:
                error = "GL_OUT_OF_MEMORY";
                break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:
                error = "GL_INVALID_FRAMEBUFFER_OPERATION";
                break;
            case GL_CONTEXT_LOST:
                error = "GL_CONTEXT_LOST";
                break;
            }
            std::cerr << "[GL ERROR] " << error << std::endl;
        }

        glfwPollEvents();

        int width = 0;
        int height = 0;
        glfwGetWindowSize(window_, &width, &height);
        clouds.setScreenSize(width, height);

        static int oldState = GLFW_RELEASE;
        int newState = glfwGetMouseButton(window_, GLFW_MOUSE_BUTTON_LEFT);
        if (newState == GLFW_RELEASE && oldState == GLFW_PRESS)
        {
            double xpos = 0;
            double ypos = 0;
            glfwGetCursorPos(window_, &xpos, &ypos);
            fireworks.spawn(glm::vec2(xpos, ypos), static_cast<float>(height));
        }
        oldState = newState;

        clouds.update();
        fireworks.update();

        glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        nightsky.draw(glm::vec2(width, height));
        clouds.draw();
        fireworks.draw();

        glfwSwapBuffers(window_);
    }
}

void Engine::cleanUp()
{
    nightsky.clear();
    clouds.clear();
    fireworks.clear();
    ResourceManager::clear();
    glfwDestroyWindow(window_);
    glfwTerminate();
}
