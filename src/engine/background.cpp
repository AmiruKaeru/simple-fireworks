#include "background.h"

#include "resource_manager.h"
#include "utils.h"

Background::Background()
    : VAO_(0)
    , VBO_(0)
{
}

void Background::setup()
{
    ResourceManager::loadShaderFromSource(R"(
        #version 330 core
        layout (location = 0) in vec2 aPos;
        out vec2 texCoords;

        uniform mat4 projection;
        uniform mat4 model;

        void main()
        {
            texCoords = aPos;
            gl_Position = projection * model * vec4(aPos, 0.0, 1.0);
        }
    )", R"(
        #version 330 core
        in vec2 texCoords;
        out vec4 fragColor;

        uniform sampler2D sprite;

        void main()
        {
            fragColor = texture(sprite, texCoords);
        }
    )", "background");

    ResourceManager::loadTexture(std::string(getPath() + "\\textures\\nightsky.jpg").c_str(), GL_FALSE, "background");

    GLfloat vertex_quad[] = {
        0.0f, 1.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f
    };

    glGenVertexArrays(1, &VAO_);
    glGenBuffers(1, &VBO_);
    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_quad), vertex_quad, GL_STATIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), static_cast<GLvoid *>(0));
    glBindVertexArray(0);
}

void Background::draw(const glm::vec2 &screenSize)
{
    auto shader = ResourceManager::getShader("background").use();

    glm::mat4 model(1);
    model = glm::scale(model, glm::vec3(screenSize, 1.0f));

    shader.setMatrix4("model", model);

    ResourceManager::getTexture("background").bind();

    glBindVertexArray(VAO_);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

void Background::clear()
{
    glDeleteVertexArrays(1, &VAO_);
    glDeleteBuffers(1, &VBO_);
}