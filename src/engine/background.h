#pragma once

#include <GL/gl3w.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Background
{
public:
    Background();

    void setup();
    void draw(const glm::vec2 &screenSize);
    void clear();

private:
    GLuint VAO_;
    GLuint VBO_;
};