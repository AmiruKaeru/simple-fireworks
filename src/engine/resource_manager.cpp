﻿#include "resource_manager.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

Shader ResourceManager::loadShaderFromSource(const GLchar *vertexSource, const GLchar *fragmentSource, const std::string &name)
{
    shaders[name] = Shader(vertexSource, fragmentSource);
    return shaders[name];
}

Shader ResourceManager::getShader(const std::string &name)
{
    return shaders[name];
}

Texture ResourceManager::loadTexture(const GLchar *file, GLboolean alpha, const std::string &name)
{
    Texture texture;

    if (alpha)
    {
        texture.setInternalFormat(GL_RGBA);
        texture.setImageFormat(GL_RGBA);
    }

    int width;
    int height;
    int nrchannels;
    unsigned char *image = stbi_load(file, &width, &height, &nrchannels, 0);
    texture.generate(width, height, image);
    stbi_image_free(image);

    textures[name] = texture;
    return texture;
}

Texture ResourceManager::getTexture(const std::string &name)
{
    return textures[name];
}

void ResourceManager::clear()
{
    for (auto shader : shaders)
    {
        glDeleteProgram(shader.second.getID());
    }

    for (auto texture : textures)
    {
        auto id = texture.second.getID();
        glDeleteTextures(1, &id);
    }
}

std::map<std::string, Shader> ResourceManager::shaders;
std::map<std::string, Texture> ResourceManager::textures;