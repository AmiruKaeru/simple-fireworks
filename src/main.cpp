#include <iostream>

#include "Engine/engine.h"

int main(int argc, char **argv)
{
    try
    {
        Engine firework;
        firework.run();
    }
    catch (const std::runtime_error &e)
    {
        std::cerr << "[ERROR] " << e.what() << std::endl;
        return -1;
    }
    return 0;
}